from random import randint

username = input("Hi! What is your name? ")

# My attempt at incorporating the streth goals of this project

listofmonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

numberofdaysinmonth = [31, 28, 31, 30, 31, 30, 31 ,31, 30, 31, 30, 31]


for numberoftry in range(5):

    month__being_guessed_in_number = (randint(1, 12))

    feedback = input("Guess " + str(numberoftry + 1) + ": " + username + ", were you born in " + listofmonths[month__being_guessed_in_number - 1] + " " + str(randint(1, numberofdaysinmonth[month__being_guessed_in_number - 1])) + ", " + str(randint(1924, 2004)) + "? \nyes or no? ")

    if (feedback == "yes"):
        print("I knew it!")
        exit()
        #break
    elif(numberoftry == 4):
        print("I have other things to do. Good bye.")
        exit()
        #break
    elif (feedback == "no"):
        print("Drat! Lemme try again!")
    else:
        print("I'll assume that was a no. Lemme try again, and next time be sure to type either 'yes' or 'no'!")

# The below code works for the minimum requirements of the project and was already submitted on GitLab. The above code is my attempt at the Stretch goals

# for numberoftry in range(5):
#     feedback = input("Guess " + str(numberoftry + 1) + ": " + username + ", were you born in " + str(randint(1, 12)) + " / " + str(randint(1924, 2004)) + "? \nyes or no? ")

#     if (feedback == "yes"):
#         print("I knew it!")
#         exit()
#         #break
#     elif(numberoftry == 4):
#         print("I have other things to do. Good bye.")
#         exit()
#         #break
#     elif (feedback == "no"):
#         print("Drat! Lemme try again!")
#     else:
#         print("I'll assume that was a no. Lemme try again, and next time be sure to type either 'yes' or 'no'!")